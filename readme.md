# nginx-poc

## resources

- https://medium.com/@chadsaun/mitigating-a-ddos-attack-with-ingress-nginx-and-kubernetes-12f309072367
- https://inmediatum.com/en/blog/engineering/ddos-attacks-prevention-nginx/
- https://stackoverflow.com/questions/64425924/rate-limiting-based-on-url-and-path-in-kubernetes
- https://github.com/kubernetes/ingress-nginx/blob/master/docs/user-guide/nginx-configuration/annotations.md#rate-limiting
