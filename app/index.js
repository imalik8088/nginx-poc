const format = require('date-fns/format');
const express = require('express');
const app = express();

const PORT = 5000;

app.get('/', (req, res) => {
  const current_ts = format(new Date(), "yyyy-MM-dd'T'HH:mm:ss.SSSxxx");
  res.json({"current timestamp": current_ts})
})

app.listen(PORT, () => console.log(`Server is up and running ${PORT}`));
